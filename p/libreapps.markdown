---
layout: default
title: "Libreapps"
category: pre-uni
---

Libreapps was my attempt at creating free software webapps. At it's most complete state, I had a semi-usable webmail client (that I built myself, very much inspired by GMail) and an RSS client (mostly code borrowed from another client) and single authentication system.

Unhappy with the way I'd structured things, I've tried restarting this project mutltiple times, but never got very far.

The website is currently disabled (libreapps.com redirects here).
