---
layout: default
title: "CUCaTS"
category: uni
---

I was on the committee of the [Cambridge University Computing and Technology Society (CUCaTS)](http://cucats.soc.srcf.net/).

I've helped run the CUCaTS puzzlehunt in 2012 and 2013.
http://cucats.soc.srcf.net/puzzlehunt
https://github.com/Bjwebb/CUCaTS-Puzzlehunt/
