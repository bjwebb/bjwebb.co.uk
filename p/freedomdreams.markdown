---
layout: default
title: "Freedom Dreams"
category: pre-uni
---

For several years, Freedom Dreams was my webblog. Due to server changes it dropped offline, and I have decided not to reinstate it, since most of the information on it is in some way out of date.
