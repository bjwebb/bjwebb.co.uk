---
layout: default
title: Events
---

### Some information about events

An incomplete list of events I have attended. 

#### 2012

* August - [Young Rewired State](/p/young-rewired-state/)
* August - [Data Development Challenge](http://developmentdatachallenge.org/events/london/) where I helped write [some code to investigate IATI Data Quality](https://github.com/markbrough/IATI-Data-Quality)
* December - [Rewired State: Wellcome Trust Health Hackday](http://hacks.rewiredstate.org/events/wthack-2012/)

#### 2013

#### 2014

* January - IATI TAG (Technical Advisory Group) meeting in Montreal

#### 2015

* January - FOSDEM
* February - FLOSSUK Unconference
